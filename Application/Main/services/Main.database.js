var db = openDatabase("PersonelDatabase2", "1.0", "VERİTABANI PERSONEL", 4 * 1024 * 1024);
create();
create_plan();

function create() {
    return new Promise(function(resolve, reject) {

        db.transaction(function(tx) {
            tx.executeSql("CREATE TABLE IF NOT EXISTS KISILER (id INTEGER PRIMARY KEY,name VARCHAR(100), mail VARCHAR(100),authority VARCHAR(100),date VARCHAR(100),password VARCHAR(100))")
            resolve();
        });
    });
}

function create_plan() {
    return new Promise(function(resolve, reject) {
        db.transaction(function(tx) {
            tx.executeSql("CREATE TABLE IF NOT EXISTS PlanT (id INTEGER PRIMARY KEY, by_name VARCHAR(100), baslik VARCHAR(50) ,bilgi VARCHAR(800), baslangic_tarihi DATATIME, bitis_tarihi DATATIME,type_name VARCHAR(50))")
            resolve();
        });
    });
}

function insert_plan(by, baslik, bilgi, baslangic_tarihi, bitis_tarihi, type) {
    return new Promise(function(resolve, reject) {
        db.transaction(function(tx) {
                tx.executeSql("INSERT INTO PlanT (by_name,baslik,bilgi,baslangic_tarihi,bitis_tarihi,type_name) VALUES(?,?,?,?,?,?)", [by, baslik, bilgi, baslangic_tarihi, bitis_tarihi, type]);
                resolve();

            }),
            function() {
                reject();
            }
    });
}

function delete_plan(text) {
    return new Promise(function(resolve, reject) {
        db.transaction(function(tx) {
            tx.executeSql("DELETE FROM PlanT WHERE bilgi=?", [text]);
            resolve();
        });
    });
}

function delete_plan_title(title) {
    return new Promise(function(resolve, reject) {
        db.transaction(function(tx) {
            tx.executeSql("DELETE FROM PlanT WHERE baslik=?", [title]);
            resolve();
        });
    });
}

function who_plan(by) {
    return new Promise(function(resolve, reject) {
        db.transaction(function(tx) {
            tx.executeSql("SELECT * FROM PlanT WHERE by_name=?", [by], function(tx, result) {
                resolve(result);
            });
        });
    });
}

function insert(name, mail, authority, date, password) {
    return new Promise(function(resolve, reject) {
        db.transaction(function(tx) {
                tx.executeSql("INSERT INTO KISILER (name,mail,authority,date,password) VALUES(?,?,?,?,?)", [name, mail, authority, date, password]);

                resolve();
            }),
            function() {
                reject();
            }
    });
}

function select() {
    return new Promise(function(resolve, reject) {
        db.transaction(function(tx) {
            tx.executeSql("SELECT * FROM KISILER", [], function(tx, result) {
                resolve(result);
            });
        });
    });
}


function vt_delete(id) {
    return new Promise(function(resolve, reject) {
        db.transaction(function(tx) {
            tx.executeSql("DELETE FROM KISILER WHERE id=?", [id]);
            resolve();
        });
    });
}

function vt_update(name, mail, authority, date, password, id) {
    return new Promise(function(resolve, reject) {
        db.transaction(function(tx) {
            tx.executeSql("UPDATE KISILER SET name=? ,mail=? ,authority=?,date=? ,password=? WHERE id=?", [name, mail, authority, date, password, id]);
            resolve();
        });
    });
}

function table_delete() {
    return new Promise(function(resolve, reject) {
        db.transaction(function(tx) {
            tx.executeSql("DELETE FROM KISILER")
            resolve();
        });
    });
}

function user(mail, password) {
    return new Promise(function(resolve, reject) {
        db.transaction(function(tx) {
            tx.executeSql("SELECT * FROM KISILER WHERE mail=? AND password=? ", [mail, password], function(tx, result) {
                resolve(result);
            });

        });
    });
}

function nameQuery(mail) {
    return new Promise(function(resolve, reject) {
        db.transaction(function(tx) {
            tx.executeSql("SELECT name,authority FROM KISILER WHERE mail=?", [mail], function(tx, result) {
                resolve(result);
            });
        });
    });
}

function authority(mail) {
    return new Promise(function(resolve, reject) {
        db.transaction(function(tx) {
            tx.executeSql("SELECT authority FROM KISILER WHERE mail=?", [mail], function(tx, result) {
                resolve(result);
            });
        });
    });
}