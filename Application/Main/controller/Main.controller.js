sap.ui.define([
        "sap/ui/core/mvc/Controller",
        "sap/m/MessageToast",
        "sap/m/MessageBox",
    ],
    function(Controller, MessageToast, MessageBox) {
        "use strict";
        var _this;

        return Controller.extend("SapUI5Tutorial.Application.Main.controller.Main", {
            onInit: function() {
                _this = this;
                _this.kayitOku();
                var mail = localStorage.getItem("Mail");
                nameQuery(mail).then(function(result) {
                    var rows = result.rows;
                    for (var i = 0; i < rows.length; i++) {
                        localStorage.setItem("Name", rows[i].name);
                        localStorage.setItem("Yetki", rows[i].authority);

                    }
                })


                if (localStorage.getItem("Yetki") == "Leader") {
                    window.location = "#/STAFF";

                } else {
                    if (localStorage.getItem("Giris") == "true") {
                        window.location = "#/Plan"
                        setTimeout(() => {
                            MessageBox.alert("Personel Ekleme Sayfasına Gitmek İçin Yetkiniz Bulunmamaktadır..")
                        }, 1000);
                    }
                }

            },
            onListItemPress: function(oEvent) {
                var sToPageId = oEvent.getParameter("listItem").getCustomData()[0].getValue();
                this.getSplitAppObj().toDetail(this.createId(sToPageId));
                this.kayitOku();
            },

            onPressModeBtn: function(oEvent) {
                var sSplitAppMode = oEvent.getSource().getSelectedButton().getCustomData()[0].getValue();
                this.getSplitAppObj().setMode(sSplitAppMode);
                MessageToast.show("Split Container mode is changed to: " + sSplitAppMode, { duration: 5000 });
            },

            getSplitAppObj: function() {
                var result = this.byId("SplitAppDemo");
                if (!result) {
                    Log.info("SplitApp object can't be found");
                }
                return result;
            },
            Kaydet: function(evt) {
                _this.local();
                var name = this.getView().byId("name").getValue();
                var mail = this.getView().byId("mail").getValue();
                var authority = this.getView().byId("authority").getValue();
                var date = this.getView().byId("datetimepicker").getValue();
                var password = this.getView().byId("password").getValue();

                if (name == "") {
                    MessageBox.warning("Adı Boş Giremezsiniz...");

                } else if (mail == "") {
                    MessageBox.warning("Maili Boş Giremezsiniz...");

                } else if (authority == "") {
                    MessageBox.warning("Yetkiyi Boş Giremezsiniz...");

                } else if (date == "") {
                    MessageBox.warning("Tarihi Boş Giremezsiniz...");

                } else if (password == "") {
                    MessageBox.warning("Şifreyi Boş Giremezsiniz...");

                } else {
                    insert(name, mail, authority, date, password).then(function() {
                        console.log("Kişi Ekleme İşlemi Başarılı");

                        _this.getView().byId("name").setValue("");
                        _this.getView().byId("mail").setValue("");
                        _this.getView().byId("authority").setValue("");
                        _this.getView().byId("datetimepicker").setValue("");
                        _this.getView().byId("password").setValue("");

                        MessageBox.success(name + "  Kişisi Rehbere Eklendi.");

                        _this.kayitOku();
                    });
                }
            },
            kayitOku: function() {
                _this.local();
                select().then(function(result, rows) {
                    console.log("Select işlemi Başarılı");
                    var list = _this.getView().byId('person');
                    var rows = result.rows;
                    list.removeAllItems();
                    /**
                    var add = new sap.m.StandardListItem({
                        title: "Kişi Ekle",
                        press: _this.kisiEkle,
                        type: "Active"
                    });
                    list.addItem(add);
                    */
                    for (var i = 0; i < rows.length; i++) {

                        var stl = new sap.m.StandardListItem({

                            title: rows[i].name,
                            description: rows[i].mail,
                            info: rows[i].authority,
                            type: "Active",
                            wrapping: true,
                            press: _this.kisiAktar,
                            icon: "sap-icon://account",

                            customData: [
                                new sap.ui.core.CustomData({
                                    key: "id",
                                    value: "" + rows[i].id + "",
                                    writeToDom: true
                                }),
                                new sap.ui.core.CustomData({
                                    key: "date",
                                    value: "" + rows[i].date + "",
                                    writeToDom: true
                                }), new sap.ui.core.CustomData({
                                    key: "password",
                                    value: "" + rows[i].password + "",
                                    writeToDom: true
                                }),
                                new sap.ui.core.CustomData({
                                    key: "name",
                                    value: "" + rows[i].name + "",
                                    writeToDom: true
                                })
                            ]
                        });
                        list.addItem(stl);
                    }
                });
            },
            kisiAktar: function(oEvent) {
                _this.local();
                _this.getView().byId("name").setValue(oEvent.getSource().mProperties.title);
                _this.getView().byId("mail").setValue(oEvent.getSource().mProperties.description);
                _this.getView().byId("authority").setValue(oEvent.getSource().mProperties.info);


                _this.id = oEvent.getSource().data("id");
                _this.date = oEvent.getSource().data("date");
                _this.password = oEvent.getSource().data("password");
                _this.name = oEvent.getSource().data("name");

                _this.getView().byId("password").setValue(_this.password)
                _this.getView().byId("datetimepicker").setValue(_this.date);

                $("#__xmlview0--btn_sil-inner").show();
                $("#__xmlview0--btn_guncelle").show();
                $("#__xmlview0--btn_kaydet-inner").hide();

            },
            kisiEkle: function() {
                _this.local();
                this.getView().byId("name").setValue("");
                this.getView().byId("mail").setValue("");
                this.getView().byId("datetimepicker").setValue("");
                this.getView().byId("password").setValue("");

                $("#__xmlview0--btn_sil-inner").hide();
                $("#__xmlview0--btn_guncelle").hide();
                $("#__xmlview0--btn_kaydet-inner").show();

            },

            delete: function(id) {
                vt_delete(id).then(function() {
                    console.log("Silme işlemi başarılı");

                    _this.getView().byId("name").setValue("");
                    _this.getView().byId("mail").setValue("");
                    _this.getView().byId("datetimepicker").setValue("");
                });
            },
            kisiSil: function() {
                _this.local();
                _this.delete(_this.id);
                MessageBox.success("Kişi Silindi.")
                _this.kayitOku();
            },
            Guncelle: function() {
                _this.local();
                var id = _this.id;
                var name = _this.getView().byId("name").getValue();
                var mail = _this.getView().byId("mail").getValue();
                var password = _this.getView().byId("password").getValue();
                var authority = _this.getView().byId("authority").getValue();
                var date = _this.getView().byId("datetimepicker").getValue();

                vt_update(name, mail, authority, date, password, id).then(function() {
                    _this.local();
                    console.log("GÜNCELLEME İŞLEMİ BAŞARILI");
                    _this.kayitOku();
                    MessageBox.success(name + " Kişisi Güncellendi.");
                });
            },
            out: function() {
                var x = confirm("Çıkmak İstediğinize Emin Misiniz ?");

                if (x) {
                    window.location = "";

                } else {
                    window.location = "#/STAFF";

                }

            },
            kGiris: function() {
                window.location = "#/UserLogin"
                window.location.reload();
            },
            userLogin: function() {
                _this.local();

                var mail = _this.getView().byId("kullaniciMail").getValue();
                var password = _this.getView().byId("kullaniciSifre").getValue();

                if (mail == "sametkarakaya14@gmail.com" && password == "123456") {
                    localStorage.setItem("Mail", mail);
                    localStorage.setItem("Password", password);

                    var saat = new Date();
                    var setupNow = saat.getHours() - 1;
                    localStorage.setItem('time', setupNow);
                    window.location = "#/STAFF";
                    window.location.reload();

                } else {
                    user(mail, password).then(function(result) {
                        if (result.rows.length > 0) {
                            var saat = new Date();
                            var setupNow = saat.getHours() - 1;
                            localStorage.setItem('time', setupNow);
                            window.location = "#/Plan";
                            localStorage.setItem("Mail", mail);
                            localStorage.setItem("Password", password);
                            localStorage.setItem("Giris", true);
                        } else {
                            MessageBox.alert("Girdiğiniz bilgiler ile eşleşen herhangi bir kullanıcı mevcut değil.");
                            _this.getView().byId("kullaniciMail").setValue("");
                            _this.getView().byId("kullaniciSifre").setValue("");
                        }
                    });

                }
                setTimeout(() => {
                    window.location.reload()
                }, 1000);
            },
            local: function() {
                var saat = new Date();
                var setupTime = localStorage.getItem('time');
                if (saat.getHours() - setupTime > 1) {
                    localStorage.clear();
                    localStorage.setItem('time', null);
                    MessageBox.alert("Oturum Zaman Aşımına Uğradı");
                    window.location = "";
                }
            },
            handleInfoMessageBoxPress2: function() {
                this.local();
                var mail = localStorage.getItem("Mail");
                authority(mail).then(function(result) {
                    var rows = result.rows;
                    MessageBox.information("İsim : " + localStorage.getItem("Name") + "\n \n Mail : " + localStorage.getItem("Mail") + "\n " + "\n Şifre  :" + localStorage.getItem("Password") + "\n \n Yetki : " + rows[0].authority);
                })

            },
            handleWarning2MessageBoxPress: function() {
                localStorage.setItem("Yetki", null)
                MessageBox.confirm(localStorage.getItem("Name") + ", Çıkmak İstediğinize Emin Misiniz ? ", {
                    actions: [MessageBox.Action.OK, MessageBox.Action.CANCEL],
                    emphasizedAction: MessageBox.Action.OK,
                    onClose: function(sAction) {
                        if (sAction == "CANCEL") {
                            window.location = "#/STAFF";

                        } else {
                            window.location = "";
                        }
                    }
                });
            }
        });
    });