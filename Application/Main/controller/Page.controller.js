sap.ui.define([
        "sap/ui/core/Fragment",
        "sap/ui/core/mvc/Controller",
        "sap/ui/core/format/DateFormat",
        "sap/ui/model/json/JSONModel",
        "sap/ui/unified/library",
        "sap/m/MessageToast",
        "sap/m/Button",
        "sap/m/Dialog",
        "sap/m/List",
        "sap/m/StandardListItem",
        "sap/m/MessageBox",
    ],
    function(Fragment, Controller, DateFormat, JSONModel, unifiedLibrary, MessageToast, Button, Dialog, List, StandardListItem, MessageBox) {
        "use strict";

        var CalendarDayType = unifiedLibrary.CalendarDayType;
        return Controller.extend("SapUI5Tutorial.Application.Main.controller.Page", {

            onInit: function() {
                var _this = this;
                var mail = localStorage.getItem("Mail");
                var by = localStorage.getItem("Name");

                who_plan(by).then(function(result) {
                    var rows = result.rows;
                    for (var j = 0; j < rows.length; j++) {
                        var sTitle = rows[j].baslik,
                            sText = rows[j].bilgi,
                            sType = rows[j].type_name,
                            oStartDate = rows[j].baslangic_tarihi,
                            oEndDate = rows[j].bitis_tarihi,
                            id = rows[j].id;
                        _this.Plan(sTitle, sText, sType, oStartDate, oEndDate, id);
                    }
                    nameQuery(mail).then(function(result) {
                        var rows = result.rows;
                        for (var i = 0; i < rows.length; i++) {
                            _this.getView().byId("menu").setText(rows[i].name);
                            localStorage.setItem("Name", rows[i].name);
                        }
                    })

                });
                var tarih = new Date();
                var oModel = new JSONModel();
                oModel.setData({
                    startDate: new Date(tarih.getFullYear(), tarih.getMonth(), tarih.getDate()),
                    appointments: [],
                    supportedAppointmentItems: [{
                            text: "Team Meeting",
                            type: CalendarDayType.Type01
                        },
                        {
                            text: "Personal",
                            type: CalendarDayType.Type05
                        },
                        {
                            text: "Discussions",
                            type: CalendarDayType.Type08
                        },
                        {
                            text: "Out of office",
                            type: CalendarDayType.Type09
                        },
                        {
                            text: "Private meeting",
                            type: CalendarDayType.Type03
                        }
                    ]
                });

                this.getView().setModel(oModel);
                oModel = new JSONModel();
                oModel.setData({ allDay: false });
                this.getView().setModel(oModel, "allDay");
                oModel = new JSONModel();
                oModel.setData({ stickyMode: "None", enableAppointmentsDragAndDrop: true, enableAppointmentsResize: true, enableAppointmentsCreate: true });
                this.getView().setModel(oModel, "settings");



            },

            _typeFormatter: function(sType) {
                var sTypeText = "",
                    aTypes = this.getView().getModel().getData().supportedAppointmentItems;

                for (var i = 0; i < aTypes.length; i++) {
                    if (aTypes[i].type === sType) {
                        sTypeText = aTypes[i].text;
                    }
                }

                if (sTypeText !== "") {
                    return sTypeText;
                } else {
                    return sType;
                }
            },

            handleAppointmentDrop: function(oEvent) {
                var oAppointment = oEvent.getParameter("appointment"),
                    oStartDate = oEvent.getParameter("startDate"),
                    oEndDate = oEvent.getParameter("endDate"),
                    bCopy = oEvent.getParameter("copy"),
                    sAppointmentTitle = oAppointment.getTitle(),
                    oModel = this.getView().getModel(),
                    oNewAppointment;

                if (bCopy) {
                    oNewAppointment = {
                        title: sAppointmentTitle,
                        icon: oAppointment.getIcon(),
                        text: oAppointment.getText(),
                        type: oAppointment.getType(),
                        startDate: oStartDate,
                        endDate: oEndDate
                    };
                    oModel.getData().appointments.push(oNewAppointment);
                    oModel.updateBindings();
                } else {
                    oAppointment.setStartDate(oStartDate);
                    oAppointment.setEndDate(oEndDate);
                }

                MessageToast.show("Appointment with title \n'" +
                    sAppointmentTitle +
                    "'\n has been " + (bCopy ? "create" : "moved")
                );
            },
            handleAppointmentResize: function(oEvent) {
                var oAppointment = oEvent.getParameter("appointment"),
                    oStartDate = oEvent.getParameter("startDate"),
                    oEndDate = oEvent.getParameter("endDate"),
                    sAppointmentTitle = oAppointment.getTitle();

                oAppointment.setStartDate(oStartDate);
                oAppointment.setEndDate(oEndDate);

                MessageToast.show("Appointment with title \n'" +
                    sAppointmentTitle +
                    "'\n has been resized"
                );
            },
            handleAppointmentCreateDnD: function(oEvent) {
                this.local
                var oStartDate = oEvent.getParameter("startDate"),
                    oEndDate = oEvent.getParameter("endDate"),
                    sAppointmentTitle = "New Appointment",
                    oModel = this.getView().getModel(),
                    oNewAppointment = {
                        title: sAppointmentTitle,
                        startDate: oStartDate,
                        endDate: oEndDate
                    };
                oModel.getData().appointments.push(oNewAppointment);
                oModel.updateBindings();

                sap.m.MessageToast.show("Appointment with title \n'" +
                    sAppointmentTitle +
                    "'\n has been created"
                );

            },

            onExit: function() {
                this.sPath = null;
                if (this._oDetailsPopover) {
                    this._oDetailsPopover.destroy();
                }
                if (this._oNewAppointmentDialog) {
                    this._oNewAppointmentDialog.destroy();
                }
            },

            handleViewChange: function() {
                MessageToast.show("'viewChange' event fired.");
            },

            handleAppointmentSelect: function(oEvent) {
                var oAppointment = oEvent.getParameter("appointment"),
                    oStartDate,
                    oEndDate,
                    oTrimmedStartDate,
                    oTrimmedEndDate,
                    bAllDate,
                    oModel;

                if (oAppointment === undefined) {
                    return;
                }

                oStartDate = oAppointment.getStartDate();
                oEndDate = oAppointment.getEndDate();
                oTrimmedStartDate = new Date(oStartDate);
                oTrimmedEndDate = new Date(oEndDate);
                bAllDate = false;
                oModel = this.getView().getModel("allDay");

                if (!oAppointment.getSelected()) {
                    this._oDetailsPopover.close();
                    return;
                }

                this._setHoursToZero(oTrimmedStartDate);
                this._setHoursToZero(oTrimmedEndDate);

                if (oStartDate.getTime() === oTrimmedStartDate.getTime() && oEndDate.getTime() === oTrimmedEndDate.getTime()) {
                    bAllDate = true;
                }

                oModel.getData().allDay = bAllDate;
                oModel.updateBindings();

                if (!this._oDetailsPopover) {
                    Fragment.load({
                            id: "popoverFrag",
                            name: "SapUI5Tutorial.Application.Main.view.Details",
                            controller: this
                        })
                        .then(function(oPopoverContent) {
                            this._oDetailsPopover = oPopoverContent;
                            this._oDetailsPopover.setBindingContext(oAppointment.getBindingContext());
                            this.getView().addDependent(this._oDetailsPopover);
                            this._oDetailsPopover.openBy(oAppointment);
                        }.bind(this));
                } else {
                    this._oDetailsPopover.setBindingContext(oAppointment.getBindingContext());
                    this._oDetailsPopover.openBy(oAppointment);
                }
            },

            handleMoreLinkPress: function(oEvent) {
                var oDate = oEvent.getParameter("date"),
                    oSinglePlanningCalendar = this.getView().byId("SPC1");

                oSinglePlanningCalendar.setSelectedView(oSinglePlanningCalendar.getViews()[2]);

                this.getView().getModel().setData({ startDate: oDate }, true);
            },

            handleEditButton: function(oEvent) {

                this._oDetailsPopover.close();
                this.sPath = this._oDetailsPopover.getBindingContext().getPath();
                this._arrangeDialogFragment("PLAN GÜNCELLEME");
                var title = this._oDetailsPopover.mProperties.title;
                delete_plan_title(title);

            },

            handlePopoverDeleteButton: function(oEvent) {
                var oModel = this.getView().getModel(),
                    oAppointments = oModel.getData().appointments,
                    oAppointment = this._oDetailsPopover._getBindingContext().getObject();
                this._oDetailsPopover.close();
                oAppointments.splice(oAppointments.indexOf(oAppointment), 1);
                var text = oAppointment.text;
                delete_plan(text).then(function() {
                    console.log("Silme işlemi başarılı");
                });
                oModel.updateBindings();
            },

            _arrangeDialogFragment: function(sTitle) {
                if (!this._oNewAppointmentDialog) {
                    Fragment.load({
                            id: "dialogFrag",
                            name: "SapUI5Tutorial.Application.Main.view.Modify",
                            controller: this
                        })
                        .then(function(oDialog) {
                            this._oNewAppointmentDialog = oDialog;
                            this.getView().addDependent(this._oNewAppointmentDialog);
                            this._arrangeDialog(sTitle);
                        }.bind(this));
                } else {
                    this._arrangeDialog(sTitle);
                }
            },

            _arrangeDialog: function(sTitle) {
                this._setValuesToDialogContent();
                this._oNewAppointmentDialog.setTitle(sTitle);
                this._oNewAppointmentDialog.open();
            },

            _setValuesToDialogContent: function() {
                var oAllDayAppointment = (Fragment.byId("dialogFrag", "allDay")),
                    sStartDatePickerID = oAllDayAppointment.getSelected() ? "DPStartDate" : "DTPStartDate",
                    sEndDatePickerID = oAllDayAppointment.getSelected() ? "DPEndDate" : "DTPEndDate",
                    oTitleControl = Fragment.byId("dialogFrag", "appTitle"),
                    oTextControl = Fragment.byId("dialogFrag", "moreInfo"),
                    oTypeControl = Fragment.byId("dialogFrag", "appType"),
                    oStartDateControl = Fragment.byId("dialogFrag", sStartDatePickerID),
                    oEndDateControl = Fragment.byId("dialogFrag", sEndDatePickerID),
                    oEmptyError = { errorState: false, errorMessage: "" },
                    oContext,
                    oContextObject,
                    oSPCStartDate,
                    sTitle,
                    sText,
                    oStartDate,
                    oEndDate,
                    sType;


                if (this.sPath) {
                    oContext = this._oDetailsPopover.getBindingContext();
                    oContextObject = oContext.getObject();
                    sTitle = oContextObject.title;
                    sText = oContextObject.text;
                    oStartDate = oContextObject.startDate;
                    oEndDate = oContextObject.endDate;
                    sType = oContextObject.type;
                } else {
                    sTitle = "";
                    sText = "";
                    if (this._oChosenDayData) {
                        oStartDate = this._oChosenDayData.start;
                        oEndDate = this._oChosenDayData.end;
                        delete this._oChosenDayData;
                    } else {
                        oSPCStartDate = this.getView().byId("SPC1").getStartDate();
                        oStartDate = new Date(oSPCStartDate);
                        oStartDate.setHours(this._getDefaultAppointmentStartHour());
                        oEndDate = new Date(oSPCStartDate);
                        oEndDate.setHours(this._getDefaultAppointmentEndHour());
                    }
                    oAllDayAppointment.setSelected(false);
                    sType = "Type01";
                }

                oTitleControl.setValue(sTitle);
                oTextControl.setValue(sText);
                oStartDateControl.setDateValue(oStartDate);
                oEndDateControl.setDateValue(oEndDate);
                oTypeControl.setSelectedKey(sType);
                this._setDateValueState(oStartDateControl, oEmptyError);
                this._setDateValueState(oEndDateControl, oEmptyError);
                this.updateButtonEnabledState(oStartDateControl, oEndDateControl, this._oNewAppointmentDialog.getBeginButton());
            },

            handleDialogOkButton: function() {
                var bAllDayAppointment = (Fragment.byId("dialogFrag", "allDay")).getSelected(),
                    sStartDate = bAllDayAppointment ? "DPStartDate" : "DTPStartDate",
                    sEndDate = bAllDayAppointment ? "DPEndDate" : "DTPEndDate",
                    sTitle = Fragment.byId("dialogFrag", "appTitle").getValue(),
                    sText = Fragment.byId("dialogFrag", "moreInfo").getValue(),
                    sType = Fragment.byId("dialogFrag", "appType").getSelectedItem().getKey(),
                    oStartDate = Fragment.byId("dialogFrag", sStartDate).getDateValue(),
                    oEndDate = Fragment.byId("dialogFrag", sEndDate).getDateValue(),
                    oModel = this.getView().getModel(),
                    by = localStorage.getItem("Name"),
                    type = Fragment.byId("dialogFrag", "appType").getSelectedItem().getText(),
                    sDate = Fragment.byId("dialogFrag", sStartDate).getValue(),
                    eDate = Fragment.byId("dialogFrag", sEndDate).getValue(),
                    sAppointmentPath;

                if (sTitle == "") {
                    MessageBox.warning("Başlık Boş Girilemez..")
                } else if (sText == "") {
                    MessageBox.warning("İçerik Boş Girilemez..")
                } else if (oStartDate == "") {
                    MessageBox.warning("Başlangıç Tarihi Boş Girilemez..")
                } else if (oEndDate == "") {
                    MessageBox.warning("Bitiş Tarihi Boş Girilemez..")
                } else if (sType == "") {
                    MessageBox.warning("Plan Tipi Boş Girilemez..")
                } else {
                    this.local();


                    insert_plan(by, sTitle, sText, oStartDate, oEndDate, sType).then(function() {
                        console.log("Plan Ekleme İşlemi Başarılı.");
                    });

                    if (Fragment.byId("dialogFrag", sStartDate).getValueState() !== "Error" &&
                        Fragment.byId("dialogFrag", sEndDate).getValueState() !== "Error") {
                        if (this.sPath) {
                            sAppointmentPath = this.sPath;
                            oModel.setProperty(sAppointmentPath + "/title", sTitle);
                            oModel.setProperty(sAppointmentPath + "/text", sText);
                            oModel.setProperty(sAppointmentPath + "/type", sType);
                            oModel.setProperty(sAppointmentPath + "/startDate", oStartDate);
                            oModel.setProperty(sAppointmentPath + "/endDate", oEndDate);
                        } else {
                            oModel.getData().appointments.push({
                                title: sTitle,
                                text: sText,
                                type: sType,
                                startDate: oStartDate,
                                endDate: oEndDate
                            });
                        }
                        oModel.updateBindings();
                        this._oNewAppointmentDialog.close();
                    }
                }

            },
            formatDate: function(oDate) {
                if (oDate) {
                    var iHours = oDate.getHours(),
                        iMinutes = oDate.getMinutes(),
                        iSeconds = oDate.getSeconds();

                    if (iHours !== 0 || iMinutes !== 0 || iSeconds !== 0) {
                        return DateFormat.getDateTimeInstance({ style: "medium" }).format(oDate);
                    } else {
                        return DateFormat.getDateInstance({ style: "medium" }).format(oDate);
                    }
                }
            },
            handleDialogCancelButton: function() {
                this.sPath = null;
                this._oNewAppointmentDialog.close();
            },
            handleCheckBoxSelect: function(oEvent) {
                var bSelected = oEvent.getSource().getSelected(),
                    sStartDatePickerID = bSelected ? "DTPStartDate" : "DPStartDate",
                    sEndDatePickerID = bSelected ? "DTPEndDate" : "DPEndDate",
                    oOldStartDate = Fragment.byId("dialogFrag", sStartDatePickerID).getDateValue(),
                    oNewStartDate = new Date(oOldStartDate),
                    oOldEndDate = Fragment.byId("dialogFrag", sEndDatePickerID).getDateValue(),
                    oNewEndDate = new Date(oOldEndDate);

                if (!bSelected) {
                    oNewStartDate.setHours(this._getDefaultAppointmentStartHour());
                    oNewEndDate.setHours(this._getDefaultAppointmentEndHour());
                } else {
                    this._setHoursToZero(oNewStartDate);
                    this._setHoursToZero(oNewEndDate);
                }
                sStartDatePickerID = !bSelected ? "DTPStartDate" : "DPStartDate";
                sEndDatePickerID = !bSelected ? "DTPEndDate" : "DPEndDate";
                Fragment.byId("dialogFrag", sStartDatePickerID).setDateValue(oNewStartDate);
                Fragment.byId("dialogFrag", sEndDatePickerID).setDateValue(oNewEndDate);
            },

            _getDefaultAppointmentStartHour: function() {
                return 9;
            },

            _getDefaultAppointmentEndHour: function() {
                return 10;
            },

            _setHoursToZero: function(oDate) {
                oDate.setHours(0, 0, 0, 0);
            },

            handleAppointmentCreate: function() {
                this._createInitialDialogValues(this.getView().byId("SPC1").getStartDate());
            },

            handleHeaderDateSelect: function(oEvent) {
                this._createInitialDialogValues(oEvent.getParameter("date"));
            },

            _createInitialDialogValues: function(oDate) {
                var oStartDate = new Date(oDate),
                    oEndDate = new Date(oStartDate);

                oStartDate.setHours(this._getDefaultAppointmentStartHour());
                oEndDate.setHours(this._getDefaultAppointmentEndHour());
                this._oChosenDayData = { start: oStartDate, end: oEndDate };
                this.sPath = null;
                this._arrangeDialogFragment("PLAN EKLEME ");
            },

            handleStartDateChange: function(oEvent) {
                var oStartDate = oEvent.getParameter("date");
                MessageToast.show("'startDateChange' event fired.\n\nNew start date is " + oStartDate.toString());
            },

            updateButtonEnabledState: function(oDateTimePickerStart, oDateTimePickerEnd, oButton) {
                var bEnabled = oDateTimePickerStart.getValueState() !== "Error" &&
                    oDateTimePickerStart.getValue() !== "" &&
                    oDateTimePickerEnd.getValue() !== "" &&
                    oDateTimePickerEnd.getValueState() !== "Error";

                oButton.setEnabled(bEnabled);
            },

            handleDateTimePickerChange: function(oEvent) {
                var oDateTimePickerStart = Fragment.byId("dialogFrag", "DTPStartDate"),
                    oDateTimePickerEnd = Fragment.byId("dialogFrag", "DTPEndDate"),
                    oStartDate = oDateTimePickerStart.getDateValue(),
                    oEndDate = oDateTimePickerEnd.getDateValue(),
                    oErrorState = { errorState: false, errorMessage: "" };

                if (!oStartDate) {
                    oErrorState.errorState = true;
                    oErrorState.errorMessage = "Please pick a date";
                    this._setDateValueState(oDateTimePickerStart, oErrorState);
                } else if (!oEndDate) {
                    oErrorState.errorState = true;
                    oErrorState.errorMessage = "Please pick a date";
                    this._setDateValueState(oDateTimePickerEnd, oErrorState);
                } else if (!oEvent.getParameter("valid")) {
                    oErrorState.errorState = true;
                    oErrorState.errorMessage = "Ivalid date";
                    if (oEvent.oSource.sId === oDateTimePickerStart.sId) {
                        this._setDateValueState(oDateTimePickerStart, oErrorState);
                    } else {
                        this._setDateValueState(oDateTimePickerEnd, oErrorState);
                    }
                } else if (oStartDate && oEndDate && (oEndDate.getTime() <= oStartDate.getTime())) {
                    oErrorState.errorState = true;
                    oErrorState.errorMessage = "Start date should be before End date";
                    this._setDateValueState(oDateTimePickerStart, oErrorState);
                    this._setDateValueState(oDateTimePickerEnd, oErrorState);
                } else {
                    this._setDateValueState(oDateTimePickerStart, oErrorState);
                    this._setDateValueState(oDateTimePickerEnd, oErrorState);
                }

                this.updateButtonEnabledState(oDateTimePickerStart, oDateTimePickerEnd, this._oNewAppointmentDialog.getBeginButton());
            },

            handleDatePickerChange: function() {
                var oDatePickerStart = Fragment.byId("dialogFrag", "DPStartDate"),
                    oDatePickerEnd = Fragment.byId("dialogFrag", "DPEndDate"),
                    oStartDate = oDatePickerStart.getDateValue(),
                    oEndDate = oDatePickerEnd.getDateValue(),
                    bEndDateBiggerThanStartDate = oEndDate.getTime() <= oStartDate.getTime(),
                    oErrorState = { errorState: false, errorMessage: "" };

                if (oStartDate && oEndDate && bEndDateBiggerThanStartDate) {
                    oErrorState.errorState = true;
                    oErrorState.errorMessage = "Start date should be before End date";
                }
                this._setDateValueState(oDatePickerStart, oErrorState);
                this._setDateValueState(oDatePickerEnd, oErrorState);
                this.updateButtonEnabledState(oDatePickerStart, oDatePickerEnd, this._oNewAppointmentDialog.getBeginButton());
            },

            _setDateValueState: function(oPicker, oErrorState) {
                if (oErrorState.errorState) {
                    oPicker.setValueState("Error");
                    oPicker.setValueStateText(oErrorState.errorMessage);
                } else {
                    oPicker.setValueState("None");
                }
            },

            handleOpenLegend: function(oEvent) {
                this.local();
                var oSource = oEvent.getSource();

                if (!this._oLegendPopover) {
                    Fragment.load({
                        id: "Legend",
                        name: "SapUI5Tutorial.Application.Main.view.Legend",
                        controller: this
                    }).then(function(oPopoverContent) {
                        this._oLegendPopover = oPopoverContent;
                        this.getView().addDependent(this._oLegendPopover);
                        this._oLegendPopover.openBy(oSource);
                    }.bind(this));
                } else if (this._oLegendPopover.isOpen()) {
                    this._oLegendPopover.close();
                } else {
                    this._oLegendPopover.openBy(oSource);
                }
            },
            out: function() {
                var x = confirm("Çıkmak İstediğinize Emin Misiniz ?");
                if (x) {
                    window.location = "";
                } else {
                    window.location = "#/Plan";
                }
            },
            onDialogPress: function() {
                this.local();
                if (!this.pressDialog) {
                    this.pressDialog = new Dialog({
                        title: "User Information",
                        content: new List({
                            items: {
                                path: "/ProductCollection",
                                template: new StandardListItem({
                                    title: name,
                                    counter: "{Quantity}"
                                })
                            }
                        }),
                        beginButton: new Button({
                            type: "Emphasized",
                            text: "OK",
                            press: function() {
                                this.pressDialog.close();
                            }.bind(this)
                        }),
                        endButton: new Button({
                            text: "Close",
                            press: function() {
                                this.pressDialog.close();
                            }.bind(this)
                        })
                    });

                    //to get access to the global model
                    this.getView().addDependent(this.pressDialog);
                }

                this.pressDialog.open();
            },
            local: function() {
                var saat = new Date();
                var setupTime = localStorage.getItem('time');
                if (saat.getHours() - setupTime > 1) {
                    localStorage.clear();
                    localStorage.setItem('time', null);
                    MessageBox.warning("Oturum Zaman Aşımına Uğradı");
                    window.location = "";
                }
            },
            handleInfoMessageBoxPress: function() {
                this.local();
                var mail = localStorage.getItem("Mail");
                authority(mail).then(function(result) {
                    var rows = result.rows;
                    MessageBox.information("İsim : " + localStorage.getItem("Name") + "\n \n Mail : " + localStorage.getItem("Mail") + "\n " + "\n Şifre  :" + localStorage.getItem("Password") + "\n \n Yetki : " + rows[0].authority);
                });

            },
            Plan: function(sTitle, sText, sType, oStartDate, oEndDate, id) {
                this.local();
                var oModel = this.getView().getModel(),
                    sAppointmentPath;
                if (this.sPath) {
                    sAppointmentPath = this.sPath;
                    oModel.setProperty(sAppointmentPath + "/title", sTitle);
                    oModel.setProperty(sAppointmentPath + "/text", sText);
                    oModel.setProperty(sAppointmentPath + "/type", sType);
                    oModel.setProperty(sAppointmentPath + "/startDate", oStartDate);
                    oModel.setProperty(sAppointmentPath + "/endDate", oEndDate);
                } else {
                    oModel.getData().appointments.push({
                        title: sTitle,
                        text: sText,
                        type: sType,
                        startDate: new Date(oStartDate),
                        endDate: new Date(oEndDate)
                    });
                }
                oModel.updateBindings();
            },
            handleWarning2MessageBoxPress: function() {
                this.local();
                MessageBox.confirm(localStorage.getItem("Name") + ", Çıkmak İstediğinize Emin Misiniz ? ", {
                    actions: [MessageBox.Action.OK, MessageBox.Action.CANCEL],
                    emphasizedAction: MessageBox.Action.OK,

                    onClose: function(sAction) {
                        if (sAction == "CANCEL") {
                            window.location = "#/Plan";
                        } else {
                            localStorage.setItem("Giris", false);
                            window.location = "";
                        }
                    }
                });
            }

        });
    });